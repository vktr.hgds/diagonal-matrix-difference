package com.hegedusviktor;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

//practice of 2d lists
public class DiagonalDifference implements DiagonalDifferenceMethods {

    //private data
    private List<List<Integer>> inputList;
    private List<Integer> left;
    private List<Integer> right;
    private List<Integer> sumAndDiff;


    public DiagonalDifference() {
        this.inputList = new ArrayList<>();
        this.left = new ArrayList<>();
        this.right = new ArrayList<>();
        this.sumAndDiff = new ArrayList<>();
    }


    //run the app
    public void run () {
        inputList = fillList(4);
        printList();
        diagonalDifference();
        printLeftDiagonal();
        printRightDiagonal();
        printDifference();
    }


    //print the abs difference of the sum of the left & right diagonal values
    private void printDifference () {
        System.out.println("\nDIFFERNCE\n|"+sumAndDiff.get(1) + " - " + sumAndDiff.get(2) + "| = " + sumAndDiff.get(0));
    }


    //print the sum of the left diagonal
    private void printLeftDiagonal () {

        System.out.println("\nLEFT DIAGONAL SUM: ");
        int counter = 0;
        while (counter < getSize().get(0)) {
            System.out.print(counter < getSize().get(0) - 1 ? left.get(counter) + " + " : left.get(counter));
            counter++;
        }
        System.out.println(" = " + sumAndDiff.get(1));
    }


    //print the sum of the right diagonal
    private void printRightDiagonal () {

        System.out.println("\nRIGHT DIAGONAL SUM");
        int counter = 0;
        Iterator<Integer> rightListIterator = right.listIterator();

        while (rightListIterator.hasNext()) {
            System.out.print(counter < getSize().get(0) - 1 ? rightListIterator.next() + " + " : rightListIterator.next());
            counter++;
        }

        System.out.println(" = " + sumAndDiff.get(2));
    }


    //get the diagonal difference of the 2d list
    private List<Integer> diagonalDifference () {

        int row = getSize().get(0);
        int col = getSize().get(1);
        int leftDiagonal = 0, rightDiagonal = 0, m = row - 1, k = 0;

        for (int i = 0; i < row; i++) {

            if (m < 0) m = row - 1;

            for (int j = 0; j < col; j++) {

                if (i == j) {
                    leftDiagonal += inputList.get(i).get(j);
                    left.add(inputList.get(i).get(j));
                }

                if (j == m) {
                    rightDiagonal += inputList.get(i).get(m);
                    right.add(inputList.get(i).get(j));
                }
            }
            m--;
        }

        int diff =  abs(leftDiagonal, rightDiagonal);
        sumAndDiff.add(diff);
        sumAndDiff.add(leftDiagonal);
        sumAndDiff.add(rightDiagonal);

        return sumAndDiff;
    }


    //abs value of the integers
    private int abs (int a, int b) {
       return a >= b ? a - b : b - a;
    }


    //fill the 2d list with random values
    public List<List<Integer>> fillList (int size) {
        Random rand = new Random();
        inputList = new ArrayList<>();

        for (int i = 0; i < size; i++) {

            List<Integer> innerList = new ArrayList<>();
            inputList.add(innerList);

            for (int j = 0; j < size; j++) {
                int n = rand.nextInt(20);
                innerList.add(n);
            }
        }

        return inputList;
    }


    //print list to console
    public void printList () {
        int rows = getSize().get(0);
        int cols = getSize().get(1);
        String space = " - ";

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                System.out.print(j < cols - 1 ? inputList.get(i).get(j) + space: inputList.get(i).get(j));
            }
            System.out.println("");
        }
    }

    //get the size of the list (number of rows & columns)
    private List<Integer> getSize () {

        int rowCount = 0;
        int colCount = 0;

        for (List<Integer> row : inputList) {
            colCount = 0;
            for (Integer col : row) {
                colCount++;
            }
            rowCount++;
        }

        List<Integer> sizes = new ArrayList<>();
        sizes.add(rowCount);
        sizes.add(colCount);

        return sizes;
    }

}