package com.hegedusviktor;

import java.util.List;

public interface DiagonalDifferenceMethods {

    List<List<Integer>> fillList (int size);
    void printList();
}
